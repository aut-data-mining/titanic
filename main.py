# required libraries
import timeit

import pandas as pd
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from tabulate import tabulate
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

"""
DATA SOURCE: https://www.kaggle.com/c/titanic/data
"""

# Resolve "setting with copy warning"
pd.options.mode.chained_assignment = None

MAX_DEPTH = 10
# "gini" or "entropy"
IMPURITY_MEASURE = "gini"


def read_test_train_data():
    """
    Read train and test file from input file directory

    :return: train_data, test_data
    """

    input_data = pd.read_csv("./input_data/titanic_data.csv")
    train_data, test_data = train_test_split(input_data, test_size=0.2, random_state=0)
    return train_data, test_data


def show_data(data):
    """
    Show data as a table

    :param data: data that loaded
    :return: nothing
    """
    print(tabulate(data, headers='keys', tablefmt='psql', showindex=False))
    print()


def preprocess(data):
    """
    Preprocess data likes removing some columns and converting text value to integer value

    :param data: the input data
    :return: preprocessed data
    """

    processed_data = data

    # Remove columns "PassengerId", "Name", "Cabin", "Ticket"
    del processed_data['PassengerId']
    del processed_data['Name']
    del processed_data['Cabin']
    del processed_data['Ticket']

    # convert text value to integer value in column "Sex"
    processed_data['Sex'] = processed_data['Sex'].apply(lambda x: +1 if x == "male" else -1)

    # Convert value to text value to  integer value
    # NOTE: The number of data with null value is very small. suppose "Q" for them!
    # processed_data = processed_data.dropna(subset=['Embarked'])
    processed_data['Embarked'] = processed_data['Embarked'].apply(lambda x: 1 if x == "S" else (2 if x == "C" else 3))

    column_means = processed_data.mean()
    processed_data = processed_data.fillna(column_means)

    # This below code specify number of null value in data set
    # print(processed_data.isnull().value_counts())

    return processed_data


def create_decision_tree_model(data):
    """
    Create a classifier model with Decision tree approach
    
    LIBRARY: https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html

    :param data: prepared train data
    :return: decision tree model
    """

    # Create Decision Tree classifier object
    clf = DecisionTreeClassifier(max_depth=MAX_DEPTH, criterion=IMPURITY_MEASURE)

    # Specify X and Y
    x_train = data.iloc[:, 1:]
    y_train = data.iloc[:, 0]

    # Train Decision Tree Classier
    clf = clf.fit(x_train, y_train)

    return clf


def create_random_forest_model(data):
    """
    Create a classifier model with Random Forest approach

    LIBRARY: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html

    :param data: prepared train data
    :return: random forest model
    """

    # Create Random Forest classifier object
    clf = RandomForestClassifier(max_depth=MAX_DEPTH,  criterion=IMPURITY_MEASURE, random_state=0)

    # Specify X and Y
    x_train = data.iloc[:, 1:]
    y_train = data.iloc[:, 0]

    # Train Random Forest Classifier
    clf = clf.fit(x_train, y_train)

    return clf


def create_linear_svm_model(data):
    """
    Create a classifier model with SVM (linear kernel function)

    LIBRARY: https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html

    :param data: prepared train data
    :return: svm model
    """

    # Create linear SVM classifier object
    clf = svm.SVC(kernel='linear', C=1)

    # Specify X and Y
    x_train = data.iloc[:, 1:]
    y_train = data.iloc[:, 0]

    # Train Random Forest Classifier
    clf = clf.fit(x_train, y_train)

    return clf


def test_classifier_model(clf, test_data):
    """
    Test a classifier with test data

    :param clf: classifier model
    :param test_data: test data
    :return: nothing
    """

    # Specify X and Y
    x_test = test_data.iloc[:, 1:]
    y_test = test_data.iloc[:, 0]

    # Predict labels
    predicts = clf.predict(x_test)

    # Calculate accuracy
    accuracy = accuracy_score(y_test, predicts)
    print(f"Accuracy = {accuracy*100}%")


def show_decision_tree(clf):
    """
    Show decision tree with visual format

    :param clf: model
    :return: nothing
    """

    plt.figure(figsize=(25, 10))
    plot_tree(clf,
              feature_names=['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked'],
              class_names='PassengerId',
              filled=True,
              rounded=True,
              fontsize=14)

    plt.show()


def run():
    """ run the program! """
    # Read train and test data
    train_data, test_data = read_test_train_data()

    # Preprocess data
    train_data = preprocess(data=train_data)
    test_data = preprocess(data=test_data)

    # Show data for better sense
    show_data(train_data)

    start = timeit.default_timer()

    # Create a decision tree model
    clf_decision_tree = create_decision_tree_model(train_data)

    stop = timeit.default_timer()
    learning_time = stop - start

    start = timeit.default_timer()

    # Test model with test data
    test_classifier_model(clf_decision_tree, test_data)

    stop = timeit.default_timer()
    testing_time = stop - start

    print(f'Learning Time: {int(learning_time*1000)} mS')
    print(f'Testing Time: {int(testing_time*1000)} mS')

    # Show decision tree with visual format
    show_decision_tree(clf_decision_tree)

    start = timeit.default_timer()

    # Create a random forest model
    clf_random_forest = create_random_forest_model(data=train_data)

    stop = timeit.default_timer()
    learning_time = stop - start

    start = timeit.default_timer()

    # Test model with test data
    test_classifier_model(clf_random_forest, test_data)

    stop = timeit.default_timer()
    testing_time = stop - start

    print(f'Learning Time: {int(learning_time*1000)} mS')
    print(f'Testing Time: {int(testing_time*1000)} mS')

    # Create a linear svm model
    clf_linear_svm = create_linear_svm_model(data=train_data)

    # Test model with test data
    test_classifier_model(clf_linear_svm, test_data)


if __name__ == '__main__':
    run()
